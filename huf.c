#include<stdio.h>
#include<stdlib.h>

void lireFichier( FILE* fichier){
  char ligne[500];
  while( fgets(ligne,500,fichier) != NULL ){
    printf( "%s" , ligne);
  }
}

int main(int argc,char** argv){
    
  FILE* fichier = fopen(argv[1],"r");
  
  if (fichier != NULL){
    lireFichier(fichier);
    fclose(fichier);
  }else{
    printf("%s\n","Ce fichier n'est pas accessible");
  }
  
  return 0;
  
}
